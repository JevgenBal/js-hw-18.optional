"use strict"

function cloneObject(obj) {
  let cloned = {};

  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      let value = obj[key];
      if (typeof value === "object") {
        cloned[key] = cloneObject(value);
      } else {
        cloned[key] = value;
      }
    }
  }
  return cloned;
}